#include "functional.hpp"

namespace openpifpaf
{
	float Clip(float n, float lower, float upper)
	{
		return std::max(lower, std::min(n, upper));
	}
	
	float ScalarValueClipped(float* field, int row, int col, float x, float y)
	{
   		float _x = Clip(x, 0.0, col - 1);
		float _y = Clip(y, 0.0, row - 1);

		return field[col * (int)_y + (int)_x];
	}
}
