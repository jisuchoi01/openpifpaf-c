#include <iostream>
#include "utils.hpp"

int Ndto1d(const int ndim, const int* iVector, const int* ndims)
{
	if (ndim < 1 || iVector == nullptr || ndims == nullptr)
		return -1;

	if (ndim == 1)
		return iVector[0];

	int idx = 0;
	for (int i = 0; i < ndim; i++)
	{
		int tmp = iVector[i];
		//printf("%d ", iVector[i]);
		for (int j = i+1; j < ndim; j++)
		{
			tmp *= ndims[j];
			// printf("x %d ", ndims[j]);
		}
		// printf("= %d\n", tmp);
		idx += tmp;
	}

	// printf("RET = %d\n", idx);
	return idx;
}

namespace openpifpaf
{
	std::tuple<float*, int, int*> IndexField(int nShape, int* shape)
	{
		std::tuple<float*, int, int*> ret;
		std::get<1>(ret) = nShape + 1;
		std::get<2>(ret) = new int[std::get<1>(ret)];
		std::get<2>(ret)[0] = nShape;
		std::copy(shape, shape + nShape, std::get<2>(ret) + 1);

		int totalSize = 1;
		for (int i = 0; i < nShape; i++)
			totalSize *= shape[i];

		std::get<0>(ret) = new float[(totalSize * nShape)];

		// CAUTION : Assume 'shape = (2, X, X)'
		int k = 0;
		for (int i = 0; i < shape[0]; i++)
			for (int j = 0; j < shape[1]; j++)
			{
				std::get<0>(ret)[k] = (float)j;
				k++;
			}

		for (int i = 0; i < shape[0]; i++)
			for (int j = 0; j < shape[1]; j++)
			{
				std::get<0>(ret)[k] = (float)i;
				k++;
			}			
		return ret;
	}
	
	// TODO: Pleaes convert this as cuBlas nd vector
	void NormalizePif(std::vector<std::tuple<float*, int, int*>> _pif_fields)
	{
		std::tuple<float*, int, int*> jointIntensityFields = _pif_fields.at(0);
		std::tuple<float*, int, int*> jointFields = _pif_fields.at(1);
		//std::tuple<float*, int, int*> _tmp = _pif_fields.at(2);
		std::tuple<float*, int, int*> scaleFields = _pif_fields.at(3);
		
		// Expand dimension at axis 1
		std::get<1>(jointIntensityFields)++;
		std::get<2>(jointIntensityFields) = new int[std::get<1>(jointIntensityFields)];
		std::get<2>(jointIntensityFields)[1] = 1;
		std::copy(std::get<2>(_pif_fields.at(0)), std::get<2>(_pif_fields.at(0)) + 1, std::get<2>(jointIntensityFields));
		std::copy(
			std::get<2>(_pif_fields.at(0)) + 1,
			std::get<2>(_pif_fields.at(0)) + std::get<1>(_pif_fields.at(0)),
			std::get<2>(jointIntensityFields) + 2);

		int totalSize = 1;
		for (int i = 0; i < std::get<1>(jointIntensityFields); i++)
			totalSize *= std::get<2>(jointIntensityFields)[i];
		std::copy(std::get<0>(_pif_fields.at(0)), std::get<0>(_pif_fields.at(0)) + totalSize, std::get<0>(jointIntensityFields));
		
		// Expand dimension at axis 1
		std::get<1>(scaleFields)++;
		std::get<2>(scaleFields) = new int[std::get<1>(scaleFields)];
		std::get<2>(scaleFields)[1] = 1;
		std::copy(std::get<2>(_pif_fields.at(3)), std::get<2>(_pif_fields.at(3)) + 1, std::get<2>(scaleFields));
		std::copy(
			std::get<2>(_pif_fields.at(3)) + 1,
			std::get<2>(_pif_fields.at(3)) + std::get<1>(_pif_fields.at(3)),
			std::get<2>(scaleFields) + 2);

		int subArrayStart = std::get<1>(jointFields) - 2;
		int subArraySize = std::get<1>(jointFields) - subArrayStart;
		int* subArray = new int[subArraySize];
		std::copy(std::get<2>(jointFields) + subArrayStart, std::get<2>(jointFields) + std::get<1>(jointFields), subArray);
		
		std::tuple<float*, int, int*> indexFields = IndexField(subArraySize, &subArray[0]);
		
		// Expand dimension at axis 0
		std::get<1>(indexFields)++;
		int* tmpDimension = new int[std::get<1>(indexFields)];
		tmpDimension[0] = 1;
		std::copy(std::get<2>(indexFields), std::get<2>(indexFields) + std::get<1>(indexFields), tmpDimension + 1);

		delete[] std::get<2>(indexFields);
		std::get<2>(indexFields) = tmpDimension;

		totalSize = 1;
		for (int i = 1; i < std::get<1>(jointFields); i++)
			totalSize *= std::get<2>(jointFields)[i];
		for (int i = 0; i < std::get<2>(jointFields)[0]; i++)
			for (int k = 0; k < totalSize; k++)
				std::get<0>(jointFields)[i*totalSize + k] += std::get<0>(indexFields)[k];

		// Concatenate jointIntensityFields, jointFields, scaleFields
		std::tuple<float*, int, int*> ret;
		std::get<1>(ret) = std::get<1>(jointIntensityFields);
		std::get<2>(ret) = new int[std::get<1>(ret)];
		std::get<2>(ret)[0] = std::get<2>(jointIntensityFields)[0];
		std::get<2>(ret)[1] = std::get<2>(jointIntensityFields)[1] + std::get<2>(jointFields)[1] + std::get<2>(scaleFields)[1];
		std::copy(std::get<2>(jointIntensityFields) + 2,
				  std::get<2>(jointIntensityFields) + std::get<1>(jointIntensityFields),
				  std::get<2>(ret) + 2);

		// Find out total needed memory for alloc
		totalSize = 1;
		for (int i = 0; i < std::get<1>(ret); i++)
			totalSize *= std::get<2>(ret)[i];

		// Alloc, new space
		std::get<0>(ret) = new float[totalSize];
		int* coord = new int[std::get<1>(ret)]();
		for (int i = 0, curIdx = 0, copyEnd = 0; i < std::get<2>(ret)[0]; i++)
		{
			// Concatenate jointIntensityFields first
			coord[1] = std::get<2>(jointIntensityFields)[1];
			copyEnd = Ndto1d(std::get<1>(jointIntensityFields), coord, std::get<2>(jointIntensityFields));
			std::copy(std::get<0>(jointIntensityFields) + i * copyEnd, std::get<0>(jointIntensityFields) + copyEnd, std::get<0>(jointIntensityFields) + curIdx);
			curIdx += copyEnd;

			// Concatenate jointFields first
			coord[1] = std::get<2>(jointFields)[1];
			copyEnd = Ndto1d(std::get<1>(jointFields), coord, std::get<2>(jointFields));
			// std::copy(std::get<0>(jointFields), std::get<0>(jointFields) + copyEnd, std::get<0>(jointFields) + curIdx);
			curIdx += copyEnd;
			
			// Concatenate scaleFields first
			coord[1] = std::get<2>(scaleFields)[1];
			copyEnd = Ndto1d(std::get<1>(scaleFields), coord, std::get<2>(scaleFields));
			// std::copy(std::get<0>(scaleFields), std::get<0>(scaleFields) + copyEnd, std::get<0>(scaleFields) + curIdx);
			curIdx += copyEnd;
		}
	}
	
	void NormalizePaf(std::vector<std::tuple<float*, int, int*>> _paf_fields)
	{
	}
}
