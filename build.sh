#! /bin/bash

# Default Var
export OPENPIFPAT_C_VERSION="0.0.1"
export ANACONDA3_PATH=$HOME/anaconda3
export PYTHON_VERSION="$(find $ANACONDA3_PATH/envs/openpifpaf/include/ -name python3.* -type d | rev | cut -d'/' -f 1 | rev)"
export PYTHON_SITE_PKG_VERSION=${PYTHON_VERSION:0:9}

UPLOAD_DIR="/home/dlinano"
HOST_NAME="10.122.65.226"
USER="dlinano"
SETUP="n"
COMPILE="y"
UPLOAD="n"
DEBUG=""
PROFILE=""

# Argparse
while test $# -gt 0
do
    case "$1" in
        -h | --help)
			echo "Use --run option if you want to run application"
            ;;
		--hostname)
			HOST_NAME=$2
			;;
		-S)
			SETUP="y"
            ;;
		-C)
			COMPILE="y"
            ;;
		-U)
			UPLOAD="y"
			;;
		-D)
			DEBUG="-DCMAKE_BUILD_TYPE=Debug"
            ;;
		-P)
			PROFILE="-DPROFILE:BOOL=ON"
            ;;
		--* | *)
			;;
    esac
    shift
done

# Setup Compile Env
if [ "${SETUP}" == "y" ];then
	sudo apt-get -y install g++-aarch64-linux-gnu
	if [ ! -d ./lib/torch ];then
		mkdir ./lib/torch
	fi
	scp -r $USER@$HOST_NAME:/usr/local/lib/python3.6/dist-packages/torch/lib/* ./lib/torch
	if [ ! -f ./lib/torch/libtorch.so ];then
		echo "[ERROR-CODE-0001] 라이브러리 인스톨에 실패했습니다. Jetson에서 필요한 패키지를 설치해주세요."
		exit -1
	fi
fi

# Compile
if [ "${COMPILE}" == "y" ];then
	cmake $DEBUG $PROFILE .
	make -j8
#	./run
fi

# Upload via scp
if [ "${UPLOAD}" == "y" ];then
	if [ ! -d ./openpifpaf-c-$OPENPIFPAT_C_VERSION ];then
	   mkdir openpifpaf-c-$OPENPIFPAT_C_VERSION
	fi
	cp -r ./bin/ ./openpifpaf-c-$OPENPIFPAT_C_VERSION/
	cp -r ./lib/ ./openpifpaf-c-$OPENPIFPAT_C_VERSION/
	cp ./install.sh ./openpifpaf-c-$OPENPIFPAT_C_VERSION/
	scp -r ./openpifpaf-c-$OPENPIFPAT_C_VERSION/ $USER@$HOST_NAME:$UPLOAD_DIR
	rm -rf openpifpaf-c-$OPENPIFPAT_C_VERSION
fi

exit 0
