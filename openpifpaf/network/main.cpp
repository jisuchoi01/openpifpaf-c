#include <iostream>
#include <memory>


#include "torch/script.h"
#include "torch/torch.h"
#include "test.hpp"


int main(void)
{
	test();
	torch::Tensor tensor = torch::rand({2, 3});
	std::cout << tensor << std::endl;
	
	// load model
	try
	{
		std::shared_ptr<torch::jit::script::Module> module = torch::jit::load("/home/jisu/Downloads/pifpaf.pt");
	}
	catch (const c10::Error& e)
	{
		std::cerr << "error loading the model\n";
		return -1;
	}
}
