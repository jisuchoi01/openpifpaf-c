import ctypes

import logging
import torch
import torchvision

from . import basenetworks, heads
from ..data import COCO_KEYPOINTS, COCO_PERSON_SKELETON, DENSER_COCO_PERSON_CONNECTIONS, HFLIP

class CShell(torch.nn.Module):
    def __init__(self, base_net, head_nets, *, head_names=None, head_strides=None, process_heads=None, cross_talk=0.0):
        self.obj = clib.Shell_new(cross_talk)

    def forward(self, *args):
        return head_outputs



class Shell(torch.nn.Module):
    def __init__(self, base_net, head_nets,
                 head_names=None, head_strides=None, process_heads=None, cross_talk=0.0):
        super(Shell, self).__init__()
        self.base_net = base_net
        self.head_nets = torch.nn.ModuleList(head_nets)
        self.head_names = head_names or [
            h.shortname for h in head_nets
        ]
        self.head_strides = head_strides or [
            base_net.input_output_scale // (2 ** getattr(h, '_quad', 0))
            for h in head_nets
        ]
        self.process_heads = process_heads
        self.cross_talk = cross_talk

    def forward(self, *args):
        image_batch = args[0]

        if self.training and self.cross_talk:
            rolled_images = torch.cat((image_batch[-1:], image_batch[:-1]))
            image_batch += rolled_images * self.cross_talk

        x = self.base_net(image_batch)
        head_outputs = [hn(x) for hn in self.head_nets]

        if self.process_heads is not None:
            head_outputs = self.process_heads(*head_outputs)

        return head_outputs


def model_migration(net_cpu):
    model_defaults(net_cpu)

    for m in net_cpu.modules():
        if not isinstance(m, torch.nn.Conv2d):
            continue
        if not hasattr(m, 'padding_mode'):  # introduced in PyTorch 1.1.0
            m.padding_mode = 'zeros'

    if not hasattr(net_cpu, 'process_heads'):
        net_cpu.process_heads = None

    if not hasattr(net_cpu, 'head_strides'):
        net_cpu.head_strides = [
            net_cpu.base_net.input_output_scale // (2 ** getattr(h, '_quad', 0))
            for h in net_cpu.head_nets
        ]

    if not hasattr(net_cpu, 'head_names'):
        net_cpu.head_names = [h.shortname for h in net_cpu.head_nets]

    for head in net_cpu.head_nets:
        if not hasattr(head, 'dropout') or head.dropout is None:
            head.dropout = torch.nn.Dropout2d(p=0.0)
        if not hasattr(head, '_quad'):
            if hasattr(head, 'quad'):
                head._quad = head.quad  # pylint: disable=protected-access
            else:
                head._quad = 0  # pylint: disable=protected-access
        if not hasattr(head, 'scale_conv'):
            head.scale_conv = None
        if not hasattr(head, 'reg1_spread'):
            head.reg1_spread = None
        if not hasattr(head, 'reg2_spread'):
            head.reg2_spread = None
        if head.shortname == 'pif17' and getattr(head, 'scale_conv') is not None:
            head.shortname = 'pifs17'
        if head._quad == 1 and not hasattr(head, 'dequad_op'):  # pylint: disable=protected-access
            head.dequad_op = torch.nn.PixelShuffle(2)
        if not hasattr(head, 'class_convs') and hasattr(head, 'class_conv'):
            head.class_convs = torch.nn.ModuleList([head.class_conv])


def model_defaults(net_cpu):
    for m in net_cpu.modules():
        if isinstance(m, (torch.nn.BatchNorm1d, torch.nn.BatchNorm2d)):
            m.eps = 1e-4
            m.momentum = 0.01

def factory_from_args():
    # configure CompositeField
    heads.CompositeField.dropout_p = 0.0
    heads.CompositeField.quad = 0

    return factory(0.0, False, False, True)

def factory(
        cross_talk=0.0,
        two_scale=False,
        multi_scale=False,
        multi_scale_hflip=True):

    tmp = torch.load('resnet18.pkl')
    #torch.save(tmp['model'], '/home/dsparch/Downloads/pifpaf.pt')
    #net_cpu = torch.load('/home/dsparch/Downloads/pifpaf.pt')
    net_cpu = tmp['model']

    # initialize for eval
    net_cpu.eval()

    # normalize for backwards compatibility
    model_migration(net_cpu)

    net_cpu.cross_talk = cross_talk

    return net_cpu
