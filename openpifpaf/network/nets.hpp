#include "torch/nn/module.h"

namespace openpifpaf
{
	class Shell : public torch::nn::Module
	{
	private:
	public:
		Shell(/*base_net, head_nets, head_names, head_strides, process_heads,*/ float cross_talk=0.0);
		void forward();
	};
}

