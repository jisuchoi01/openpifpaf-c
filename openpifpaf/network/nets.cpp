#include "nets.hpp"

namespace openpifpaf
{

	Shell::Shell(/*base_net, head_nets, head_names, head_strides, process_heads,*/ float cross_talk)
		: torch::nn::Module()
	{
		
	}

	void Shell::forward()
	{
		
	}

	extern "C"
	{
		Shell* Shell_new(float cross_talk=0.0){return new Shell(/*base_net, head_nets, head_names, head_strides, process_heads,*/cross_talk);}
		void Shell_forward(Shell* shell) {shell->forward();}
	}
}




