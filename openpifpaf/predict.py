"""Predict poses for given images."""

import argparse
import json
import logging
import os

import numpy as np
import PIL
import cv2
import torch

from .network import nets
from . import decoder, transforms

def cli():
    parser = argparse.ArgumentParser(
        prog='python3 -m openpifpaf.predict',
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    
    decoder.cli(parser, force_complete_pose=False, instance_threshold=0.1, seed_threshold=0.5)
    parser.add_argument('--batch-size', default=1, type=int, help='processing batch size')
    parser.add_argument('--long-edge', default=None, type=int, help='apply preprocessing to batch images')
    parser.add_argument('--loader-workers', default=None, type=int, help='number of workers for data loading')

    args = parser.parse_args()

    if args.loader_workers is None:
        args.loader_workers = args.batch_size

    return args

if __name__ == '__main__':
    args = cli()

    # data
    image = cv2.imread('test_data/1pj9.jpg', cv2.IMREAD_COLOR)
    image_PIL = PIL.Image.fromarray(image)

    # load model
    import time

    # Model Load
    s1 = time.time()
    model_cpu = nets.factory_from_args()
    model = model_cpu.to(torch.device('cpu'))    
    model.head_names = model_cpu.head_names
    model.head_strides = model_cpu.head_strides
    e1 = time.time()

    # Preprocess image
    s2 = time.time()
    processed_image_cpu, _, __ = transforms.EVAL_TRANSFORM(image_PIL, [], None)
    processed_image = processed_image_cpu.contiguous().to('cpu', non_blocking=True)
    e2 = time.time()

    # Retrieve pifpaf decoder
    s3 = time.time()
    processor = decoder.factory_from_args(args, model, torch.device('cpu'))
    e3 = time.time()

    # Actual processing (torch.no_grad())
    s4 = time.time()
    fields = processor.fields(torch.unsqueeze(processed_image, 0))[0]
    e4 = time.time()

    # Pifpaf part
    s5 = time.time()
    keypoints, scores = processor.keypoint_sets(fields)
    e5 = time.time()
    
    print(f"{e1 - s1:0.6f}, {e2 - s2:0.6f}, {e3 - s3:0.6f}, {e4-s4:0.6f}, {e5-s5:0.6f}")
    
    # keypoints dimension = batchsize x person count x 17 (joints per person) x 3 (x, y, confidence of joints)
    for i in range(0, keypoints.shape[0]):
        person = keypoints[i]
        for j in range(0, person.shape[0]):            
            joint = person[j]
            cv2.circle(image, (joint[0], joint[1]), 5, (0, 255, 0), thickness=1, lineType=8, shift=0)

    cv2.imshow('TEST', image)
    cv2.waitKey(0)
