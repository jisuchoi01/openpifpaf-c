"""The Processor runs the model to obtain fields and passes them to a decoder."""
import io
import logging
import multiprocessing
import pstats

import numpy as np
import torch

from .utils import scalar_square_add_single
from ..functional import scalar_nonzero_clipped

import ctypes
import os
clib = ctypes.pydll.LoadLibrary(os.path.abspath('lib/libopenpifpaf.so'))

class Processor(object):
    def __init__(self,
                 model,
                 decode,
                 keypoint_threshold=0.0,
                 instance_threshold=0.0,
                 device=None,
                 suppressed_v=0.0,
                 cdecoder=None):

        self.model = model
        self.decode = decode
        self.keypoint_threshold = keypoint_threshold
        self.instance_threshold = instance_threshold
        self.device = device
        self.suppressed_v = suppressed_v
        self.cdecoder = cdecoder

    def fields(self, image_batch):
        if self.device is not None:
            image_batch = image_batch.to(self.device, non_blocking=True)

        with torch.no_grad():
            heads = self.model(image_batch)

            # to numpy
            fields = [[field.cpu().numpy() for field in head] for head in heads]

            # index by batch entry
            fields = [
                [[field[i] for field in head] for head in fields] for i in range(image_batch.shape[0])
            ]

        return fields

    def soft_nms(self, annotations):
        if not annotations:
            return annotations

        occupied = np.zeros((
            len(annotations[0].data),
            int(max(np.max(ann.data[:, 1]) for ann in annotations) + 1),
            int(max(np.max(ann.data[:, 0]) for ann in annotations) + 1),
        ), dtype=np.uint8)

        annotations = sorted(annotations, key=lambda a: -a.score())
        for ann in annotations:
            joint_scales = (np.maximum(4.0, ann.joint_scales)
                            if ann.joint_scales is not None
                            else np.ones((ann.data.shape[0]),) * 4.0)

            assert len(occupied) == len(ann.data)
            for xyv, occ, joint_s in zip(ann.data, occupied, joint_scales):
                v = xyv[2]
                if v == 0.0:
                    continue

                if scalar_nonzero_clipped(occ, xyv[0], xyv[1]):
                    xyv[2] = self.suppressed_v
                else:
                    scalar_square_add_single(occ, xyv[0], xyv[1], joint_s, 1)

        annotations = [ann for ann in annotations if np.any(ann.data[:, 2] > self.suppressed_v)]
        annotations = sorted(annotations, key=lambda a: -a.score())
        return annotations

    def keypoint_sets(self, fields):
        clib.Processor_KeypointSets.argtypes = [ctypes.c_void_p, ctypes.py_object]
        clib.Processor_KeypointSets.restype = None
        clib.Processor_KeypointSets(self.cdecoder, fields)
        return None, None

        # annotations = self.decode(fields, initial_annotations=None)
        # annotations = self.soft_nms(annotations)

        # # threshold
        # for ann in annotations:
        #     kps = ann.data
        #     kps[kps[:, 2] < self.keypoint_threshold] = 0.0
        # annotations = [ann for ann in annotations if ann.score() >= self.instance_threshold]
        # annotations = sorted(annotations, key=lambda a: -a.score())

        # keypoint_sets = [ann.data for ann in annotations]
        # scores = [ann.score() for ann in annotations]
             
        # keypoint_sets = np.array(keypoint_sets)
        # scores = np.array(scores)

        # for i in range(0, len(fields)):
        #     for j in range(0, len(fields[0])):
        #         print(fields[i][j].shape)
                
        # return keypoint_sets, scores
