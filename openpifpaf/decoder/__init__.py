"""Collections of decoders: fields to annotations."""

from .annotation import Annotation
from .factory import cli, factory_from_args
from .pif import Pif
from .pifpaf import PifPaf
from .processor import Processor
