"""Utilities for decoders."""

import functools
import numpy as np



@functools.lru_cache(maxsize=16)
def index_field(shape):
    yx = np.indices(shape, dtype=np.float32)
    xy = np.flip(yx, axis=0)
    return xy

def normalize_paf(intensity_fields, j1_fields, j2_fields, j1_fields_logb, j2_fields_logb):
    # print(intensity_fields.shape, j1_fields.shape, j1_fields.shape, j1_fields_logb.shape, j2_fields_logb.shape)
    intensity_fields = np.expand_dims(intensity_fields, 1)
    j1_fields_b = np.expand_dims(np.exp(j1_fields_logb), 1)
    j2_fields_b = np.expand_dims(np.exp(j2_fields_logb), 1)
    index_fields = index_field(j1_fields[0, 0].shape)
    index_fields = np.expand_dims(index_fields, 0)
    j1_fields3 = np.concatenate((intensity_fields, index_fields + j1_fields, j1_fields_b), axis=1)
    j2_fields3 = np.concatenate((intensity_fields, index_fields + j2_fields, j2_fields_b), axis=1)
    paf = np.stack((j1_fields3, j2_fields3), axis=1)

    return paf


def normalize_pif(joint_intensity_fields, joint_fields, tmp, scale_fields):
    joint_intensity_fields = np.expand_dims(joint_intensity_fields.copy(), 1)
    scale_fields = np.expand_dims(scale_fields, 1)
    index_fields = index_field(joint_fields.shape[-2:])    
    index_fields = np.expand_dims(index_fields, 0)
    joint_fields = index_fields + joint_fields
    ret = np.concatenate(
        (joint_intensity_fields, joint_fields, scale_fields),
        axis=1,
    )
    print(joint_intensity_fields.shape)
    print(joint_fields.shape)
    print(scale_fields.shape)
    print(ret.shape)
    return ret

def scalar_square_add_single(field, x, y, width, value):
    minx = max(0, int(x - width))
    miny = max(0, int(y - width))
    maxx = max(minx + 1, min(field.shape[1], int(x + width) + 1))
    maxy = max(miny + 1, min(field.shape[0], int(y + width) + 1))
    field[miny:maxy, minx:maxx] += value
