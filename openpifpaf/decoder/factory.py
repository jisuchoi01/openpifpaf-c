import logging

from ..data import COCO_KEYPOINTS, COCO_PERSON_SKELETON, DENSER_COCO_PERSON_CONNECTIONS
from .pif import Pif
from .pif_hr import PifHr
from .pifpaf import PifPaf
from .processor import Processor

import ctypes
import os
clib = ctypes.cdll.LoadLibrary((os.path.abspath("lib/libopenpifpaf.so")))

def cli(parser, *,
        force_complete_pose=True,
        seed_threshold=0.2,
        instance_threshold=0.0,
        keypoint_threshold=None):
    group = parser.add_argument_group('decoder configuration')
    group.add_argument('--seed-threshold', default=seed_threshold, type=float,
                       help='minimum threshold for seeds')
    group.add_argument('--instance-threshold', type=float,
                       default=instance_threshold,
                       help='filter instances by score')
    group.add_argument('--keypoint-threshold', type=float,
                       default=keypoint_threshold,
                       help='filter keypoints by score')
    group.add_argument('--extra-coupling', default=0.0, type=float,
                       help='extra coupling')

    if force_complete_pose:
        group.add_argument('--no-force-complete-pose', dest='force_complete_pose',
                           default=True, action='store_false')
    else:
        group.add_argument('--force-complete-pose', dest='force_complete_pose',
                           default=False, action='store_true')
    group.add_argument('--profile-decoder', default=None, action='store_true',
                       help='profile decoder')

    group = parser.add_argument_group('PifPaf decoders')
    group.add_argument('--fixed-b', default=PifPaf.fixed_b, type=float,
                       help='overwrite b with fixed value, e.g. 0.5')
    group.add_argument('--pif-fixed-scale', default=PifPaf.pif_fixed_scale, type=float,
                       help='overwrite pif scale with a fixed value')
    group.add_argument('--pif-th', default=PifHr.v_threshold, type=float,
                       help='pif threshold')
    group.add_argument('--paf-th', default=PifPaf.paf_th, type=float,
                       help='paf threshold')


def factory_from_args(args, model, device=None):
    # configure PifPaf
    PifPaf.fixed_b = args.fixed_b
    PifPaf.pif_fixed_scale = args.pif_fixed_scale
    PifPaf.paf_th = args.paf_th
    PifPaf.connection_method = 'max'
    PifPaf.force_complete = args.force_complete_pose

    # configure Pif
    Pif.pif_fixed_scale = args.pif_fixed_scale

    # configure PifHr
    PifHr.v_threshold = args.pif_th

    # default value for keypoint filter depends on whether complete pose is forced
    if args.keypoint_threshold is None:
        args.keypoint_threshold = 0.001 if not args.force_complete_pose else 0.0

    decode = PifPaf(model.head_strides[-1],
                      keypoints=COCO_KEYPOINTS,
                      skeleton=COCO_PERSON_SKELETON)

    clib.PifPaf_New.argtypes = [ctypes.py_object,
                                ctypes.py_object,
                                ctypes.py_object,
                                ctypes.py_object,
                                ctypes.py_object,
                                ctypes.py_object,
                                ctypes.py_object,
                                ctypes.py_object]
    clib.PifPaf_New.restype = ctypes.c_void_p
    stride = model.head_strides[-1]
    pif_index=0
    paf_index=1
    pif_min_scale=0.0
    paf_min_distance=0.0
    paf_max_distance=None
    seed_threshold=0.2

    strides = [stride]
    pif_indices = [pif_index]
    paf_indices = [paf_index]
    pif_min_scales = pif_min_scale
    paf_min_distances = paf_min_distance
    paf_max_distances = paf_max_distance
    pif_min_scales = [pif_min_scale for _ in strides]
    paf_min_distances = [paf_min_distance for _ in strides]
    paf_max_distances = []

    cdecoder = clib.PifPaf_New(strides,
                              COCO_KEYPOINTS,
                              COCO_PERSON_SKELETON,
                              pif_indices,
                              paf_indices,
                              pif_min_scales,
                              paf_min_distances,
                              paf_max_distances)
    
    return Processor(model,
                     decode,
                     instance_threshold=args.instance_threshold,
                     keypoint_threshold=args.keypoint_threshold,
                     device=device,
                     cdecoder=cdecoder)
