import numpy as np

from ..functional import scalar_value_clipped, scalar_value

import ctypes
import os
clib  = ctypes.cdll.LoadLibrary(os.path.abspath('lib/libopenpifpaf.so'))

class CAnnotation(object):
    def __init__(self, keypoints, skeleton):
        ##### JISU #####
        if __debug__:
            import time
            s = time.time()
        clib.Annotation_New.argtypes = [ctypes.py_object, ctypes.py_object]
        clib.Annotation_New.restype = ctypes.c_void_p
        self.obj = clib.Annotation_New(keypoints, skeleton)
        if __debug__:
            e = time.time()
            print(f"{e-s:.6f} {type(self).__name__} __init__ void")

    def print_vars(self):
        clib.Annotation_PrintVars.argtypes = [ctypes.c_void_p]
        clib.Annotation_PrintVars.restype = ctypes.c_void_p
        clib.Annotation_PrintVars(self.obj)

    def add(self, joint_i, xyv):
        clib.Annotation_Add.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.py_object]
        clib.Annotation_Add.restype = ctypes.c_void_p
        self.obj = clib.Annotation_Add(self.obj, joint_i, xyv)
        return self

    def get_data(self, row, col):
        clib.Annotation_GetData.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_int]
        clib.Annotation_GetData.restype = ctypes.c_float
        return clib.Annotation_GetData(self.obj, row, col)
    
    def get_data_row(self, row):
        clib.Annotation_GetDataRow.argtypes = [ctypes.c_void_p, ctypes.c_int]
        clib.Annotation_GetDataRow.restype = ctypes.py_object
        return clib.Annotation_GetDataRow(self.obj, row)
    
    def frontier_next(self, update_frontier=False):
        clib.Annotation_FrontierNext.argtypes = [ctypes.c_void_p, ctypes.c_bool]
        clib.Annotation_FrontierNext.restype = ctypes.py_object
        return clib.Annotation_FrontierNext(self.obj, update_frontier)

    def append_decoding_order(self, item):
        if __debug__:
            import time
            s = time.time()

        clib.Annotation_AppendDecodingOrder.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_int, ctypes.py_object, ctypes.py_object]
        clib.Annotation_AppendDecodingOrder.restype = None
        clib.Annotation_AppendDecodingOrder(self.obj, item[0], item[1], tuple(item[2]), tuple(item[3]))
        if __debug__:
            e = time.time()
            print(f"{e-s:.6f} {type(self).__name__} append_decoding_order {type(item)}")

    def fill_joint_scales(self, scales, hr_scale=1.0):
        clib.Annotation_FillJointScales.argtypes = [ctypes.c_void_p,
                                                    np.ctypeslib.ndpointer(dtype=ctypes.c_float, ndim=len(scales.shape), flags='C_CONTIGUOUS'),
                                                    ctypes.c_int,
                                                    ctypes.py_object,
                                                    ctypes.c_float]
        
        clib.Annotation_FillJointScales.restype = None
        #print(f"{type(self).__name__} {11} {194} {423} {scales[11][423, 194]}")
        clib.Annotation_FillJointScales(self.obj, scales, len(scales.shape), scales.shape, hr_scale)
            
    def score(self):
        pass


class Annotation(object):
    def __init__(self, keypoints, skeleton):
        if __debug__:
            import time
            s = time.time()
        
        self.keypoints = keypoints
        self.skeleton = skeleton

        self.data = np.zeros((len(keypoints), 3), dtype=np.float32)
        self.joint_scales = None
        self.fixed_score = None
        self.decoding_order = []

        self.skeleton_m1 = (np.asarray(skeleton) - 1).tolist()

        self.score_weights = np.ones((len(keypoints),))
        self.score_weights[:3] = 3.0
        self.score_weights /= np.sum(self.score_weights)

        if __debug__:
            e = time.time()
            print(f"{e-s:.6f} {type(self).__name__} __init__ {type(keypoints), type(skeleton)}")
        
    def append_decoding_order(self, item):
        if __debug__:
            import time
            s = time.time()
        self.decoding_order.append(item)
        if __debug__:
            e = time.time()
            print(f"{e-s:.6f} {type(self).__name__} append_decoding_order {type(item)}")

    def add(self, joint_i, xyv):
        self.data[joint_i] = xyv
        return self

    def get_data_row(self, row):
        return self.data[row]

    def get_data(self, row, col):
        return self.data[row, col]

    # 1번
    def fill_joint_scales(self, scales, hr_scale=1.0):
        self.joint_scales = np.zeros((self.data.shape[0],))
        for xyv_i, xyv in enumerate(self.data):
            if xyv[2] == 0.0:
                continue

            x = np.clip(xyv[0] * hr_scale, 0.0, scales[xyv_i].shape[1] - 1)
            y = np.clip(xyv[1] * hr_scale, 0.0, scales[xyv_i].shape[0] - 1)
            scale =  scales[xyv_i][int(y), int(x)]

            self.joint_scales[xyv_i] = scale / hr_scale
            # print(self.joint_scales[xyv_i])

    def score(self):
        if self.fixed_score is not None:
            return self.fixed_score

        v = self.data[:, 2]
        return np.sum(self.score_weights * np.sort(v)[::-1])

    def frontier(self):
        list1 = [(self.data[j1i, 2], connection_i, True, j1i, j2i) for connection_i, (j1i, j2i) in enumerate(self.skeleton_m1)
                 if self.data[j1i, 2] > 0.0 and self.data[j2i, 2] == 0.0]
        list2 = [(self.data[j2i, 2], connection_i, False, j1i, j2i) for connection_i, (j1i, j2i) in enumerate(self.skeleton_m1)
                 if self.data[j2i, 2] > 0.0 and self.data[j1i, 2] == 0.0] 
        self._frontier = sorted(list1 + list2, reverse=True)
        # print(self._frontier)
           
    def frontier_next(self, update_frontier=False):
        if update_frontier:
            self.frontier()

        # print(f"{type(self).__name__} {len(self._frontier)}")
        while self._frontier:
            next_item = self._frontier.pop(0)
            forward = next_item[2]
            i_target = next_item[4] if forward else next_item[3]
            xyv_target = self.data[i_target]
            if xyv_target[2] != 0.0:
                continue

            return next_item
        return None

    
