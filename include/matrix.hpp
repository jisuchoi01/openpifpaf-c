#include <iostream>
#include <algorithm>

template <typename T>
class Matrix
{
private:
	int _row;
	int _col;
	T** _data; 

public:
	Matrix(int row, int col, T fillWith=T(0))
	{
		_row = row;
		_col = col;
		_data = new T *[row];
		for (int i = 0; i < row; i++)
		{
			_data[i] = new T[col];
			std::fill_n(_data[i], col, fillWith);
		}
	}
	
	~Matrix()
	{
		for (int i = 0; i < _row; i++)
			delete[] _data[i];
		delete[] _data;
	}

	T& operator() (int i, int j)
	{
		return _data[i][j];
	}

	void ToString()
	{
		std::cout << _row << " x " << _col << std::endl;
		for (int i = 0; i < _row; i++)
		{
			printf("%3d : ", i);
			for (int j = 0; j < _col; j++)
				printf("%lf ", _data[i][j]);
			std::cout << std::endl;
		}
	}

	T** GetData()
	{
		return _data;
	}

	void FillWithN(T N)
	{
		// for(T * iter = &_data; iter != &_data + _row * _col; iter++)
		// 	*iter = N;
		// for(T * iter = &_data; iter != &_data + _row * _col; iter++)
		// 	std::cout << *iter << ", " << std::endl;
	}

	T Sum()
	{
		T sum = T(0);
		for (int i = 0; i < _row; i++)
			for (int j = 0; j < _col; j++)
				sum += _data[i][j];
				
		return sum;
	}

	void Divide(T constant)
	{
		for (int i = 0; i < _row; i++)
			for (int j = 0; j < _col; j++)
				_data[i][j] /= constant;
	}

	int Row(void)
	{return _row;}

	int Col(void)
	{return _col;}

	int Size(void)
	{return _row * _col;}

};
