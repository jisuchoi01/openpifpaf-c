#include <cassert>
#include "utils.hpp"
#include "greedy.hpp"

namespace openpifpaf
{
	class PifPaf
	{
	public:
		static std::string connectionMethod;
		static float pafThreshold;
		static float fixed_b;

		std::vector<int> m_Strides, m_PifIndices, m_PafIndices;
		std::vector<float> m_PifMinScales, m_PafMinDistances, m_PafMaxDistances;
		std::vector<std::string> m_Keypoints;
		std::vector<std::tuple<int, int>> m_Skeleton;
		float m_SeedThreshold = 0.2;
		int m_Pif_nn = 16, m_Paf_nn = 35;

		PifPaf(std::vector<int> strides,
			   std::vector<std::string> keypoints,
			   std::vector<std::tuple<int, int>> skeleton,
			   std::vector<int> pifIndices,
			   std::vector<int> pafIndices,
			   std::vector<float> pifMinScales,
			   std::vector<float> pafMinDistances,
			   std::vector<float> pafMaxDistances,
			   float seedThreshold=0.2f);
		std::vector<Annotation> Decode(std::vector<std::vector<std::tuple<float*, int, int*>>> fields);
	};
}
