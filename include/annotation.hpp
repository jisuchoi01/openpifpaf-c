#include <string>
#include <vector>
#include <tuple>

#include "matrix.hpp"

namespace openpifpaf
{
	class Annotation
	{	
	private:
		std::vector<std::string> _keypoints;
		std::vector<std::tuple<int, int>> _skeleton, _skeleton_m1;
		std::vector<std::tuple<float, int, bool, int, int>> _frontier;

	public:
		Matrix<float> *_data;
		std::vector<std::tuple<int, int, float*, float*>> _decoding_order;
		Matrix<float> *_joint_scales, *_score_weights;

		Annotation(std::vector<std::string> keypoints, std::vector<std::tuple<int, int>> skeleton);
		~Annotation();
		Annotation* Add(int joint_i, float* xyv, int xyvLen);
		void Frontier();
		std::tuple<float, int, bool, int, int> FrontierNext(bool updateFrontier);
		void PrintVars(void);

	}; // end class Annotation
} // end namespace openpifpaf
